# Clay and Marty Data Extravaganza 

Paradigms Final Project

GET
/le/c/
No body
Return list of countries with data for each
Goes through all data of all countries and returns it

GET
/le/c/:country_name
No body
Return list of data for a country
Goes through all data of country and returns it

GET
/le/y/:year
No body
Return all stats for given year
Goes through countries and returns data for specific year

GET
/le/c/:country/:year
No body
Return all stats for given country in given year
Compares to other life expectancies and provides feedback

GET
/le/s/:sex
No body
Returns stats for all countries in all years for a given sex (male, female, or both)


DELETE
/le/
No body
Deletes entire index

Complexities:
This project was complex in mostly the nature of the data itself. The json form that the data came from was cluttered and hard to understand. The data had to then be entirely rearranged and handled in mass with the amount of entries. As seen in the code as well the data dictionary and lists are confusing and long offering many different options to show results. The complexity of the project definitely stemmed from analyzing the data and picking out the right components. It also offers mostly GET options as we believed average life expectancy is a search and learn rather than input data. The project itself was difficult in formulating these things and switching the json data into a readable format for us to parse and present through JS. The Scale of the project as well is massive as it collects the data of every single country for over a decade with different data sets within those. One can see the different country names along with each sex's life expectancy. The project difficulty definitely spanned from controlling the large amounts of data. 


