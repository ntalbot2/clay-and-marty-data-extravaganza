var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = makeNetworkCall;

function getFormInfo(){
    console.log('Start getFormInfo')
    var url = 'http://student13.cse.nd.edu:51065/le/'
    //makeNetworkCall('GET', url, '')
    var selindex = document.getElementById("select-server-address").selectedIndex;
    var sex = document.getElementById('select-server-address').options[selindex].value;

    var country = document.getElementById('country-text').value;
    var year = document.getElementById('year-text').value;
    
    
    
    if(document.getElementById('checkbox-sex-value').checked){
        url = url + 's/' + sex
    }
    else if(document.getElementById('checkbox-country-value').checked && document.getElementById('checkbox-year-value').checked){
        url = url + 'c/' + country + '/' + year
    }
    else if(document.getElementById('checkbox-country-value').checked){
        url = url + 'c/' + country
    }
    else if(document.getElementById('checkbox-year-value').checked){
        url = url + 'y/' + year
    }

    
    console.log('url down updating  ' + url);
   
    
    return url;
    
    //makeNetworkCall(action, url, MessageBody)
  

}

function makeNetworkCall(){
    console.log('Enter makeNetworkCall');
    var Parameters = getFormInfo();
    

    console.log('URL: ' + Parameters);
    
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    
    xhr.open('GET', Parameters, true) // 2 - associates request attributes with xhr
    //xhr.open('GET', New, true)
    // set up onload
    //if (xhr.readyState == 4)
//else console.error(xhr.statusText)

    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        if (xhr.readyState == 4){
            console.log(xhr.responseText);
            // do something
            updatePlaceWithResponse(xhr.responseText);
        }
        else {
            console.error(xhr.statusText)
        }
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request


}

function updatePlaceWithResponse(response_text){
    console.log('Enter updatePlace');
    var response_json = JSON.parse(response_text)
    var label2 = document.getElementById('answer-label');

    label2.innerHTML = 'Country: ' + response_json['data']['country'] + ', Year: ' + response_json['data']['year']
    

}
