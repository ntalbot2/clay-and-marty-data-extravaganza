import json
class _life_database:
        def __init__(self):
            self.life_db = dict()

        def load_db(self, life_file):
            f = open(life_file)
            raw_data = json.load(f)
            for datum in raw_data['fact']:
                country = datum['dims']['COUNTRY'].lower()
                year = datum['dims']['YEAR']
                if datum['dims']['GHO'] == 'Life expectancy at birth (years)':
                    stat_type = 'birth'
                else:
                    stat_type = 'age60'

                if datum['dims']['SEX'] == 'Both sexes':
                    sex = 'both'
                elif datum['dims']['SEX'] == 'Male':
                    sex = 'male'
                elif datum['dims']['SEX'] == 'Female':
                    sex = 'female'
                else:
                    sex = 'Error'
        
                value = datum['Value']
                dstat = dict()
                dsex = dict()
                dyear = dict()
                dstat[stat_type] = value
                dsex[sex] = dstat
                dyear[year] = dsex
                if country not in self.life_db.keys():
                    self.life_db[country] = dyear
                else:
                    if year not in self.life_db[country].keys():
                        self.life_db[country].update(dyear)
                    else:
                        if sex not in self.life_db[country][year].keys():
                            self.life_db[country][year].update(dsex)
                        else:
                            self.life_db[country][year][sex].update(dstat)

            f.close()
            

if __name__ == "__main__":
       ldb = _life_database()
       ldb.load_db('life_exp.json')

       print(ldb.life_db['mongolia'])


