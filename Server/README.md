# Clay and Marty Data Extravaganza 

Paradigms Final Project

GET
/le/c/
No body
Return list of countries with data for each
Goes through all data of all countries and returns it

GET
/le/c/:country_name
No body
Return list of data for a country
Goes through all data of country and returns it

GET
/le/y/:year
No body
Return all stats for given year
Goes through countries and returns data for specific year

GET
/le/c/:country/:year
No body
Return all stats for given country in given year
Compares to other life expectancies and provides feedback

GET
/le/s/:sex
No body
Returns stats for all countries in all years for a given sex (male, female, or both)


DELETE
/le/
No body
Deletes entire index


