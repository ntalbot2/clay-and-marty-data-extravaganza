var oneT = false;
var twoT = false;
var threeT = false;
var fourT = false;
var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = makeNetworkCall;


function getFormInfo(){
    console.log('Start getFormInfo')
    var url = 'http://student13.cse.nd.edu:51065/le/'
    //makeNetworkCall('GET', url, '')
    var selindex = document.getElementById("select-server-address").selectedIndex;
    var sex = document.getElementById('select-server-address').options[selindex].value;

    var country = document.getElementById('country-text').value;
    var year = document.getElementById('year-text').value;
    
    oneT = false;
    twoT = false;
    threeT = false;
    fourT = false;
    
    
    if(document.getElementById('checkbox-sex-value').checked){
        url = url + 's/' + sex
        oneT = true;
    }
    else if(document.getElementById('checkbox-country-value').checked && document.getElementById('checkbox-year-value').checked){
        url = url + 'c/' + country + '/' + year
        twoT = true;
    }
    else if(document.getElementById('checkbox-country-value').checked){
        url = url + 'c/' + country
        threeT = true;
    }
    else if(document.getElementById('checkbox-year-value').checked){
        url = url + 'y/' + year
        fourT = true
    }

    
    console.log('url down updating  ' + url);
   
    
    return url;
    
    //makeNetworkCall(action, url, MessageBody)
  

}

function makeNetworkCall(){
    console.log('Enter makeNetworkCall');
    var Parameters = getFormInfo();
    

    console.log('URL: ' + Parameters);
    
    var xhr = new XMLHttpRequest(); // 1 - creating request object
    
    xhr.open('GET', Parameters, true) // 2 - associates request attributes with xhr
    //xhr.open('GET', New, true)
    // set up onload
    //if (xhr.readyState == 4)
//else console.error(xhr.statusText)

    xhr.onload = function(e) { // triggered when response is received
        // must be written before send
        if (xhr.readyState == 4){
            console.log(xhr.responseText);
            // do something
            updatePlaceWithResponse(xhr.responseText);
        }
        else {
            console.error(xhr.statusText)
        }
    }

    // set up onerror
    xhr.onerror = function(e) { // triggered when error response is received and must be before send
        console.error(xhr.statusText);
    }

    // actually make the network call
    xhr.send(null) // last step - this actually makes the request


}

function updatePlaceWithResponse(response_text){
    console.log('Enter updatePlace');
    var response_json = JSON.parse(response_text)
    var label2 = document.getElementById('answer-label');
    label2.innerHTML = ' <br/>'
    var year = 2016;
    year.toString();
    if(threeT){ //If just country is selected
        while(year > 1999){
            year.toString();
            label2.innerHTML += 'Country: ' + response_json['country'] + ' Life Span ' + response_json['data'][year]['both']['birth'] + ' years in ' + year + '. <br/>'
            year = parseInt(year, 10);
            year = year - 1;
            
            console.log('Year: ' + year)
        }   
    }
    else if(twoT){ //if country and year
        console.log('Enter country and year' + response_json['data'][0]['country'])
        label2.innerHTML += response_json['data'][0]['country'] + ' Life Expectancy in ' + response_json['data'][0]['year'] + ' was ' + response_json['data'][0]['year_data']['both']['birth'] + ' years.<br/>'
    }
    else if(fourT){ // If year
    console.log('Enter year ' + response_json['data'][0]['year_data']['both']['birth'])

        for(var i = 0; i < response_json.data.length ; i++){
            label2.innerHTML += response_json['data'][i]['country'] + ' Life Expectancy in ' + response_json['data'][i]['year'] + ' was ' + response_json['data'][i]['year_data']['both']['birth'] + ' years.<br/>'
        }

    }
    else if(oneT){ //sex
        console.log('Enter sex')
        for(var y = 0; y < response_json.data.length ; y++){
            label2.innerHTML += response_json['data'][y]['country'] + ' Life Expectancy in ' + response_json['data'][y]['year'] + ' for (a) ' + response_json['data'][y]['sex'] +' was ' + response_json['data'][y]['sex_data']['birth'] + ' years.<br/>'
        }
    }


}
