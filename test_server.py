import unittest
import requests
import json

class TestLife(unittest.TestCase):

    SITE_URL = 'http://localhost:51065/le/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_country_index(self):
       r = requests.get(self.SITE_URL + 'c/')
       self.assertTrue(self.is_json(r.content.decode('utf-8')))
       resp = json.loads(r.content.decode('utf-8'))
       self.assertEqual(resp['result'], 'success')
       self.assertTrue(resp['countries'])
    
    def test_country_get(self):
       r = requests.get(self.SITE_URL + 'c/afghanistan')
       self.assertTrue(self.is_json(r.content.decode('utf-8')))
       resp = json.loads(r.content.decode('utf-8'))
       self.assertEqual(resp['result'], 'success')
       self.assertEqual(resp['COUNTRY'], 'afghanistan')
       self.assertTrue(resp['Data'])


    def test_year_get(self):
       r = requests.get(self.SITE_URL + 'y/2001')
       self.assertTrue(self.is_json(r.content.decode('utf-8')))
       resp = json.loads(r.content.decode('utf-8'))
       self.assertEqual(resp['result'], 'success')
       self.assertTrue(resp['data'])
       self.assertEqual(resp['data'][0]['year'], '2001')
    
    def test_sex_get(self):
       r = requests.get(self.SITE_URL + 's/male')
       self.assertTrue(self.is_json(r.content.decode('utf-8')))
       resp = json.loads(r.content.decode('utf-8'))
       self.assertEqual(resp['result'], 'success')
       self.assertTrue(resp['data'])
       self.assertEqual(resp['data'][0]['sex'], 'male')


    def test_yearCountry_get(self):
       r = requests.get(self.SITE_URL + 'c/afghanistan/2001')
       self.assertTrue(self.is_json(r.content.decode('utf-8')))
       resp = json.loads(r.content.decode('utf-8'))
       self.assertEqual(resp['result'], 'success')
       self.assertTrue(resp['data'])
       self.assertEqual(resp['data'][0]['year'], 2001)
       self.assertEqual(resp['data'][0]['country'], 'afghanistan')

if __name__ == "__main__":
    unittest.main()
