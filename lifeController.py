import cherrypy
import re, json
from life_library import _life_database

class LifeController(object):

        def __init__(self, ldb=None):
                if ldb is None:
                        self.ldb = life_db()
                else:
                        self.ldb = ldb

                        self.ldb.load_db('life_exp.json')
                #self.mdb.load_movies('movies.dat')

        def GET_COUNTRY(self, country_name):
                '''when GET request for country comes in'''
                output = {'result':'success'}

                country_name = country_name.lower()
                try:
                        country_data = self.ldb.life_db[country_name]
                        if country_data is not None:
                                output['country'] = country_name
                                output['year'] = country_data
                        else:
                                output['result'] = 'key_error'
                                output['message'] = 'country not found'
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def GET_COUNTRY_INDEX(self):
            '''when GET request for index of all countries is received'''
            output = {'result':'success'}

            try:
                output['countries'] = self.ldb.life_db
            except Exception as ex:
                output['result'] = 'error'
                output['message'] = str(ex)

            return json.dumps(output)


        def GET_YEAR(self, year):
                '''when GET request for Year comes in, we respond with all the information from the year'''
                output = {'result':'success'}
                year = int(year)
                output['data'] = list()

                if year < 2000 or year > 2016:
                    output['result'] = 'error'
                    output['message'] = 'Not Valid Year'
                    return json.dumps(output)
        
                try:
                    for country in self.ldb.life_db.keys():
                        year_data = self.ldb.life_db[country][str(year)]
                        year_info = {'country':country, 'year':str(year), 'year_data':year_data}
                        output['data'].append(year_info)
                except KeyError as ex:
                    output['result'] = 'error'
                    output['message'] = 'Key Error'
                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)

                return json.dumps(output)

        def GET_YearCountry(self, year, country):
                '''when GET request for One year in country'''
                output = {'result':'success'}
                year = int(year)
                output['data'] = list()
                country = country.lower()
                if year < 2000 or year > 2016:
                    output['result'] = 'error'
                    output['message'] = 'Not Valid Year'
                    return json.dumps(output)
        
                try:
                       
                    year_data = self.ldb.life_db[country][str(year)]
                    year_info = {'country':country, 'year':year, 'year_data':year_data}
                    output['data'].append(year_info)
                except KeyError as ex:
                    output['result'] = 'error'
                    output['message'] = 'Key Error'
                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex)

                return json.dumps(output)
        
        def GET_SEX(self, sex):
                '''when GET request for Sex'''
                output = {'result':'success'}
                output['data'] = list()
        
                try:
                    for country in self.ldb.life_db:
                        for year in self.ldb.life_db[country]:
                            sex_data = self.ldb.life_db[country][year][sex]
                            sex_info = {'country':country, 'year': year, 'sex':sex, 'sex_data' : sex_data}
                            output['data'].append(sex_info)
                
                except KeyError as ex:
                        output['result'] = 'error'
                        output['message'] = 'Key Error: '+sex 
                except Exception as ex:
                        output['result'] = 'error'
                        output['message'] = str(ex)

                return json.dumps(output)

        def DELETE_INDEX(self):
                '''when DELETE just wipe'''
                output = {'result' : 'success'}
                try:
                    self.ldb.life_db.clear()
                                        
                except KeyError as ex:
                    output['result'] = 'error'
                    output['message'] = 'key not found'
                except Exception as ex:
                    output['result'] = 'error'
                    output['message'] = str(ex) 
                return json.dumps(output)
