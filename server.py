import cherrypy
from lifeController import LifeController
#from resetController import ResetController
from life_library import _life_database

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"

def start_service():
    dispatcher = cherrypy.dispatch.RoutesDispatcher()


    ldb = _life_database()

    lifeController       = LifeController(ldb=ldb) 
    #resetController     = ResetController(mdb=mdb)

    dispatcher.connect('country_index_get', '/le/c/', controller=lifeController, action = 'GET_COUNTRY_INDEX', conditions=dict(method=['GET']))
    dispatcher.connect('country_get', '/le/c/:country_name', controller=lifeController, action = 'GET_COUNTRY', conditions=dict(method=['GET']))
    dispatcher.connect('year_get', '/le/y/:year', controller=lifeController, action = 'GET_YEAR', conditions=dict(method=['GET']))
    dispatcher.connect('index_delete', '/le/', controller=lifeController, action = 'DELETE_INDEX', conditions=dict(method=['DELETE']))
    dispatcher.connect('year_country_get', '/le/c/:country/:year', controller=lifeController, action = 'GET_YearCountry', conditions=dict(method=['GET']))
    dispatcher.connect('sex_get', '/le/s/:sex', controller=lifeController, action = 'GET_SEX', conditions=dict(method=['GET']))

    #dispatcher.connect('reset_put', '/reset/:movie_id', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    #dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))

    dispatcher.connect('country_index_options', '/le/c/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('country_key_options', '/le/c/:country_name', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('year_key_options', '/le/y/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('index_options', '/le/', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('year_country_key_options', '/le/c/:country/:year', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('sex_key_options', '/le/s/:sex', controller=optionsController, action = 'OPTIONS', conditions=dict(method=['OPTIONS']))


    conf = {
	'global': {
            'server.thread_pool': 5, # optional argument
	    'server.socket_host': 'localhost', # 
	    'server.socket_port': 51065, #change port number to your assigned
	    },
	'/': {
	    'request.dispatch': dispatcher,
            'tools.CORS.on': True,
	    }
    }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)

# end of start_service


if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS)
    start_service()

